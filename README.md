# Watermarking PDF files in Windows

Project Description:
This solution “draws” a text watermark on a PDF file. With the help of a dynamically created VBscript, the application is able to programmatically edit a PDF file and write a text watermark on a specified section of the PDF document.  The application only works in the Windows Operating System and on open (non-secure) PDF files.

There are input fields and list boxes used to set the watermark text, font, size, color, opacity, degrees to turn, vertical, and horizontal alignments. 

Error handling routines are set on the VBscript and FileMaker code.

Pre-requisites:

This solution…
*   Works in the Window Operating System only.
*   Needs Adobe Acrobat installed (Version 9 Standard was used).
*   Works on open (files without security) PDFs.

Also...
*   This project was designed using FileMaker 16.  Some functionality might not work properly if you try opening this file in a lower FileMaker version.
*   You have to enable the “Allow URLs to perform FileMaker scripts” option. This option allows the VBscript to interact with the solution.  To enable this option, go to File->Manage->Security->Extended Privileges then click on “fmurlscript” and finally click on the Edit… button.  Set the user access to this option and you are done.

About field names:
*   Field name starting with…   Denotes… 
*   ct_         Container field
*   vbs_        Field containing the VBscript code.
*   g_          Global field

Any feedback or comments are greatly appreciated.

Thanks,

Ed Gar